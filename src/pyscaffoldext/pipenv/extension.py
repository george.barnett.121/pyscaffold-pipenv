from typing import List

from pyscaffold.actions import Action, ActionParams, ScaffoldOpts, Structure
from pyscaffold.extensions import Extension
from pyscaffold.operations import no_overwrite
from pyscaffold.structure import merge
from pyscaffold.templates import get_template

from pyscaffoldext.pipenv import templates


class Pipenv(Extension):
    """
    This class serves as the skeleton for your new PyScaffold Extension. Refer
    to the official documentation to discover how to implement a PyScaffold
    extension - https://pyscaffold.org/en/latest/extensions.html
    """

    PIPFILE: str = "Pipfile"

    def activate(self, actions: List[Action]) -> List[Action]:
        """Activate extension. See :obj:`pyscaffold.extension.Extension.activate`."""
        actions = self.register(actions, self.add_files)
        return actions

    def add_files(self, struct: Structure, opts: ScaffoldOpts) -> ActionParams:
        """Add custom extension files. See :obj:`pyscaffold.actions.Action`"""
        template = get_template(self.PIPFILE, relative_to=templates)
        files: Structure = {self.PIPFILE: (template, no_overwrite())}
        return merge(struct, files), opts
